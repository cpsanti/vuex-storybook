import MyCounter from '../components/MyCounter.vue';
import Vue from "vue";
import VueCompositionApi from "@vue/composition-api";
import storeMock from '../__mocks__/storeMock';

Vue.use(VueCompositionApi);

export default {
    title: "MyCounter",
    component: MyCounter,
}

const Template = (args, {argTypes}) => ({
    store: storeMock.original,
    props: Object.keys(argTypes),
    components: { MyCounter },
    template: '<MyCounter v-bind="$props" />',
});

export const Standard = Template.bind({});
