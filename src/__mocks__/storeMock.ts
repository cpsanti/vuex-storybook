import Vue from 'vue'
import Vuex from 'vuex'
import { createDirectStore } from "direct-vuex";

Vue.use(Vuex)

const {
  store
} = createDirectStore({
  state: {
    count: 0
  },
  getters: {
    countx2(state) {
      return state.count * 2;
    }
  },
  mutations: {
    increment(state, {delta}) {
      state.count += delta;
    }
  },
  actions: {
    async incrementAsync({commit}) {
      await new Promise(r => setTimeout(r, 1000));
      commit("increment", {delta: 2});
    }
  },
  modules: {
  }
});

export default store;
